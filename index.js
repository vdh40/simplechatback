const routers = require('./src/routers/routers');
const express = require('express');
const port = 8080;
const host = '0.0.0.0';

class Application {
  constructor() {
    const app = express();
    require('express-ws')(app);

    routers.configRouters(app);

    app.listen(port, host);
    console.log(`Server listening on host ${host} at port ${port}!`);
  }
}

let App = new Application();