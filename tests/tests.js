const mocha = require('mocha');
const assert = require('assert');
const functions = require('../src/functions/functions');

mocha.describe('translateChatToJSON', function () {
  let messages = [{
    name: 'vlad',
    message: 'Hi!',
    date: '1111'
  },
    {
      name: 'vlad',
      message: 'How\'re u?',
      date: '1111'
    }];

  let chat = {
    messages: messages
  };

  mocha.it ('Преобразовывает объект в JSON', function () {
    assert.strictEqual(functions.translateChatToJSON(chat), '[{"from":"vlad","msg":"Hi!","date":"1111"},{"from":"vlad","msg":"How\'re u?","date":"1111"}]');
  })
});