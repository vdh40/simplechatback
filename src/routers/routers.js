const models = require('../models/models');
const express = require('express');
const mysql = require('mysql');

const dbConnection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'Ca5106381',
  database : 'chat_auth_db'
});

dbConnection.connect();

let onlineUsers = [];
let websockets = new Map();
let chatRooms = new models.ChatRooms();

function configRouters(app) {
  const jsonParser = express.json();

  app.use((request, response, next) => {
    console.log(`Request url: ${request.url}`);
    next();
  });

  app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", `${request.headers.origin}`);
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.get("/ping", (request, response) => {
    response.send('pong');
  });

  app.post("/auth", jsonParser, PostAuthEndpoint);
  app.post("/register", jsonParser, PostRegisterEndpoint);

  app.ws('/', (ws, request) => {
    if (!websockets.has(request.query.username)) {
      websockets.set(request.query.username, ws);
    }
    WSGetDialogsList(ws, request.query.username);

    ws.on('close', () => {
      websockets.delete(request.query.username);
      onlineUsers.splice(onlineUsers.indexOf(request.query.username), 1);
      websockets.forEach(socket => {
        WSGetOnlineUsersList(socket);
      });
    });

    ws.on('message', (receivedMessage) => {
      const data = JSON.parse(receivedMessage);

      switch (data.msgType) {
        case 'getOnlineUsersList':
          WSGetOnlineUsersList(ws);
          break;

        case 'getDialogsList':
          WSGetDialogsList(ws, data.username);
          break;

        case 'getDialogById':
          WSGetDialogById(ws, data.dialogId);
          break;

        case 'postMessage':
          WSPostMessage(ws, data.dialogId, data.username, data.msg);
          break;

        case 'createChat':
          WSCreateChat(ws, data.firstTalker, data.secondTalker);
          break;

        default:
          WSSendError(ws, 'Unexpected message type');
      }
    });
  });

}

function WSGetOnlineUsersList(ws) {
  ws.send(JSON.stringify({
    msgType: 'onlineUsersListUpd',
    onlineUsersList: GetOnlineUsersList()
  }));
}

function WSGetDialogsList(ws, username) {
  ws.send(JSON.stringify({
    msgType: 'dlgListUpd',
    dialogsList: GetAllChatsOfUserEndpoint(username)
  }));
}

function WSGetDialogById(ws, dialogId) {
  ws.send(JSON.stringify({
    msgType: 'dlgUpd',
    dialog: GetOneChatByIdEndpoint(dialogId)
  }));
}

function WSPostMessage(ws, dialogId, username, msg) {
  const message = new models.Message(username, msg, Date.now());
  if (!PostMessageEndpoint(dialogId, message)) {
    WSSendError(ws, 'Cannot post message');
    return;
  }

  WSGetDialogById(ws, dialogId);

  const chat = GetOneChatByIdEndpoint(dialogId);
  const companion = (chat.firstTalker === username) ? chat.secondTalker : chat.firstTalker;
  const companionWS = websockets.get(companion);

  if (companionWS) {
    WSGetDialogById(companionWS, dialogId);
  }
}

function WSSendError(ws, errText) {
  ws.send(JSON.stringify({
    msgType: 'error',
    errText: errText
  }));
}

function WSCreateChat(ws, firstTalker, secondTalker) {
  const dialogId = PostCreateChatEndpoint(firstTalker, secondTalker);

  ws.send(JSON.stringify({
    msgType: 'chatCreated',
    dlgId: dialogId
  }));

  WSGetDialogsList(ws, firstTalker);

  const chat = GetOneChatByIdEndpoint(dialogId);
  const companion = (chat.firstTalker === firstTalker) ? chat.secondTalker : chat.firstTalker;
  const companionWS = websockets.get(companion);

  if (companionWS) {
    WSGetDialogsList(companionWS, companion);
  }
}

function PostRegisterEndpoint(request, response) {
  let queryPromise = new Promise(((resolve, reject) => {
    dbConnection.query(`SELECT id_user AS id FROM users WHERE user_login = "${request.body.username}";`, function (error, results, fields) {
      if (error) {
        throw error;
      }

      if (results[0]) {
        reject(new Error('User already registered!'));
      } else {
        resolve('Ok');
        console.log(results[0]);
      }
    });
  }));

  queryPromise.then(
    (res) => {
      dbConnection.query(`INSERT INTO users (user_login, user_hash) VALUES
                        ("${request.body.username}", "${request.body.hash}");`);

      response.sendStatus(200);
    },
    (err) => {
      response.sendStatus(400);
    }
  );
}

function GetOneChatByIdEndpoint(dialogId) {
  return chatRooms.getChatById(dialogId);
}

function GetAllChatsOfUserEndpoint(username) {
  return chatRooms.getChats(username);
}

function GetOnlineUsersList() {
  return onlineUsers.reduce((acc, user) => acc.concat({username: user}), []);
}

function PostAuthEndpoint(request, response) {
  if (!request.body) {
    return response.sendStatus(400);
  }

  const isUserLogged = onlineUsers.some(value => value === request.body.username);
  if (!isUserLogged) {
    onlineUsers.push(request.body.username);
  }

  let queryPromise = new Promise(((resolve, reject) => {
    dbConnection.query(`SELECT id_user AS id FROM users WHERE user_login = "${request.body.username}" 
                            AND user_hash = "${request.body.hash}";`, function (error, results, fields) {
      if (error) {
        throw error;
      }

      if (!results[0]) {
        reject(new Error('User is not registered!'));
      }

      resolve('Ok');
      console.log(results[0]);
    });
  }));

  queryPromise.then(
    (res) => {
      websockets.forEach((ws) => {
        WSGetOnlineUsersList(ws);
      });
      response.sendStatus(200);
    },
    (err) => {
      response.sendStatus(400);
    }
  );
}

function PostMessageEndpoint(dialogId, message) {
  let chat = chatRooms.getChatById(dialogId);
  if (!chat) {
    return false;
  }

  chat.addMessage(message);

  return true;
}

function PostCreateChatEndpoint(firstTalker, secondTalker) {
  const existingChat = chatRooms.getChatByUsernames(firstTalker, secondTalker);

  if (existingChat) {
    return existingChat.id;
  }

  return chatRooms.createChat(firstTalker, secondTalker);
}

module.exports = {configRouters};