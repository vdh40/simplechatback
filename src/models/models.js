const sha256 = require('crypto-js/sha256');

class Message {
  constructor(name, message, date) {
    this.from = name;
    this.msg = message;
    this.date = date;
  }
}

class Room {
  constructor (firstTalker, secondTalker, id) {
    this.firstTalker = firstTalker;
    this.secondTalker = secondTalker;
    this.messages = [];
    this.id = id;
  };

  addMessage (message) {
    this.messages.push(message);
  }
}

class ChatRooms {
  constructor() {
    this.chatRooms = [];
  };

  createChat(firstTalker, secondTalker) {
    const id = String(sha256(firstTalker + secondTalker));

    this.chatRooms.push(new Room(firstTalker, secondTalker, id));
    return id;
  }

  getChatById(dialogId) {
    return this.chatRooms.find(value => value.id === dialogId);
  }

  getChats(username) {
    const chats = [];

    this.chatRooms.forEach((value) => {
      if ((value.firstTalker === username) || (value.secondTalker === username)) {
        chats.push({
          id: value.id,
          firstTalker: value.firstTalker,
          secondTalker: value.secondTalker
        });
      }
    });

    return chats;
  }

  getChatByUsernames(firstUsername, secondUsername) {
    return this.chatRooms.find(value => {
      const {firstTalker: first, secondTalker: second} = value;
      return (first === firstUsername && second === secondUsername) || (first === secondUsername && second === firstUsername);
    });
  }
}

module.exports = {Message, ChatRooms};